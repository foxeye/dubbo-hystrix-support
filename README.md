熔断器V1.0.0
=============
    本插件为专为dubbo服务消费者进行服务熔断封装的插件
    使用了专业的hystrix类库实现
采用默认配置集成：
=============
    直接在pom中添加依赖，零配置，熔断器即可生效
    <dependency>
        <groupId>com.ruubypay.miss</groupId>
        <artifactId>ruubypay-dubbo-hystrix-support</artifactId>
        <version>1.0.0-RELEASE</version>
    </dependency>
采用自定义配置集成
===============
一，在pom中添加依赖
-----------------    
    <dependency>
        <groupId>com.ruubypay.miss</groupId>
        <artifactId>ruubypay-dubbo-hystrix-support</artifactId>
        <version>1.0.0-RELEASE</version>
    </dependency>
二，在dubbo的配置文件中添加如下内容：
-----------------
    例如：
    <dubbo:consumer>
        <dubbo:parameter key="errorCode" value="H002" />
        <dubbo:parameter key="threadPoolCoreSize" value="20" />
        <dubbo:parameter key="circuitBreakerForceOpen" value="true" />
        <dubbo:parameter key="circuitBreakerSleepWindowInMilliseconds" value="2333" />
        <dubbo:parameter key="xxxxxxxx" value="xxxxx" />
        ......
    </dubbo:consumer>
各个配置项含义
=============
    在枚举类com.ruubypay.miss.config.ConfigKey 中已经提供了键的定义。和含义说明
    配置的键名称字符串通过每个 枚举.getKeyName() 方法获取。
    metricsRollingStatisticAlwindowMilliSeconds:
        设置统计的滚动窗口的时间段大小。该属性是线程池保持指标时间长短。默认10s，也就说，指标数据以xxx s为一个分段。
    circuitBreakerRequestVolumeThresHold:
        设置在一个滚动窗口中，打开断路器的最少请求数。比如：如果值是20，在一个窗口内（比如10秒），收到19个请求，即使这19个请求都失败了，断路器也不会打开。
    circuitBreakerSleepWindowInMilliseconds:
        设置在回路被打开，拒绝请求到再次尝试请求并决定回路是否继续打开的时间，通俗翻译：熔断器中断请求30秒后会进入半打开状态,放部分流量过去重试。
    circuitBreakerEnabled:
        是否开启熔断器，默认是true，设置为false时，整个熔断功能都不开启。
    circuitBreakerErrorThresholdPercentage:
        错误率达到多少时开启熔断保护，默认是50。
    circuitBreakerForceOpen:
        是否开启熔断器阻止所有请求，默认是false,如果开启，会导致你的程序不调用任何下游依赖。
    circuitBreakerForceClosed:
        如果该属性设置为true，强制断路器进入关闭状态，将会允许所有的请求，无视错误率。
    executionTimeoutInMilliseconds：
        调用超时时间，默认是1000ms,此配置生效需要前提条件：executionTimeoutEnabled 配置为true才行。
    executionTimeoutEnabled：
        配置为false时，使用dubbo配置的超时，配置为true时，如果dubbo的超时小于ExecutionTimeoutInMilliseconds，会生效dubbo的超时，
        如果dubbo的超时大于ExecutionTimeoutInMilliseconds，熔断器配置的ExecutionTimeoutInMilliseconds会生效
        建议配置为false，如果配置为true，则建议 EXECUTI_ONTIMEOUT_INMILLISECONDS 配置的足够大。
    threadPoolCoreSize：
        熔断器保持的线程池大小，所有调用生产组的各个接口时共享。默认值是2000
        建议配置值：服务消费者需要调用的每个服务的接口的并发个数之和稍微再多一些
        例如：用户中心只依赖了推送中心获取融云Token接口，其余的再也没依赖。需要获取融云Token并发数线上峰值200。则线程池大小设置为230即可。
        依赖的其他服务的接口越多，这个值需要设置越大
    errorCode：
        当熔断器拦截请求，执行callback逻辑，设置熔断器拦截请求后的返回ModelsReturn中的错误码。
    