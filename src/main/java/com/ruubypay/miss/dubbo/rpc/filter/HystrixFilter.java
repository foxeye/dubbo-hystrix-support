package com.ruubypay.miss.dubbo.rpc.filter;

import com.alibaba.dubbo.common.Constants;
import com.alibaba.dubbo.common.URL;
import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;
import com.ruubypay.miss.hystrix.DubboHystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * dubbo服务熔断拦截器,作为消费者，消费其他服务时，会执行此方法
 * @author chenhaiyang
 */
@Activate(group = Constants.CONSUMER)
public class HystrixFilter implements Filter {

    private static Logger logger = LoggerFactory.getLogger(HystrixFilter.class);

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        DubboHystrixCommand command = new DubboHystrixCommand(invoker, invocation);
        URL url = invoker.getUrl();
        logger.info("调用服务地址：{}",url);
        try{
            return command.execute();
        }catch(Exception e){
            logger.error("调用异常，熔断器报警：",e);
            throw new RpcException(e);
        }
    }
}
